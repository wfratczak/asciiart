package pl.edu.pwr.pp;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;


public class Conversion {

    static public BufferedImage conversionToGreyscale(BufferedImage  image){
        String pathToGreyscale = " ";
      //  BufferedImage  image = (BufferedImage)imageIcon.getImage();
        int width;
        int height;
        String dir = "\\src\\main\\resources\\";
        File currentDirectory = new File(new File(".").getAbsolutePath());
        BufferedImage BufferedImageGRAY = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        try{
            width = image.getWidth();
            height = image.getHeight();
            for(int i=0; i<height; i++){
                for(int j=0; j<width; j++){
                    Color c = new Color(image.getRGB(j, i));
                    int red = (int)(c.getRed() * 0.299);
                    int green = (int)(c.getGreen() * 0.587);
                    int blue = (int)(c.getBlue() *0.114);
                    Color newColor = new Color(red+green+blue, red+green+blue,red+green+blue);
                    BufferedImageGRAY.setRGB(j,i, newColor.getRGB());
                    //image.setRGB(j,i,newColor.getRGB());
                }
            }
            ImageFileWriter ImageFileWriter = new ImageFileWriter();
            ImageFileWriter.saveToPGMfromBufferedImage(BufferedImageGRAY, currentDirectory.getCanonicalPath() + dir + "grayscale.pgm");

            /*****przeskalowanie obrazu*******/
            ImageScaler imageScaler = new ImageScaler();
            Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
            double w = screensize.getWidth();
            double h = screensize.getHeight();

            BufferedImage resizedImageGRAY = imageScaler.resizeImage(BufferedImageGRAY, BufferedImage.TYPE_BYTE_GRAY, (int)w, (int)h );
            ImageFileWriter ImageFileWriterResize = new ImageFileWriter();
            ImageFileWriter.saveToPGMfromBufferedImage(resizedImageGRAY , currentDirectory.getCanonicalPath() + dir + "pgmScaled.pgm");

            BufferedImage resizedImageGRAY80 = imageScaler.resizeImage(BufferedImageGRAY, BufferedImage.TYPE_BYTE_GRAY, 80, (int)h );
            ImageFileWriter ImageFileWriterResize80 = new ImageFileWriter();
            ImageFileWriter.saveToPGMfromBufferedImage(resizedImageGRAY80 , currentDirectory.getCanonicalPath() + dir + "pgmScaled80.pgm");

            BufferedImage resizedImageGRAY160 = imageScaler.resizeImage(BufferedImageGRAY, BufferedImage.TYPE_BYTE_GRAY, 160, (int)h );
            ImageFileWriter ImageFileWriterResize160 = new ImageFileWriter();
            ImageFileWriter.saveToPGMfromBufferedImage(resizedImageGRAY160 , currentDirectory.getCanonicalPath() + dir + "pgmScaled160.pgm");

        }catch (Exception e){
            e.printStackTrace();
        }
        return BufferedImageGRAY;
    }

}
