package pl.edu.pwr.pp;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;

/**
 * Created by wojtekfratczak on 01.06.2016.
 */
public class ImageManager {

    /*Singleton*/
    private static ImageManager instance = null;
    protected ImageManager() {
        // Exists only to defeat instantiation.
    }
    public static ImageManager sharedManager() {
        if(instance == null) {
            instance = new ImageManager();
        }
        return instance;
    }
    private int[][] tempPgmPixels;
    private String localFilePath;
    private BufferedImage currentImage;
    private String pgmPath = "tempImage";

    String txtExtension = ".txt";
    File currentDirectory = new File(new File(".").getAbsolutePath());

    //LOADING
    public BufferedImage loadLocalImage(String path) {
        localFilePath = path;
        BufferedImage image = null;
        File file = new File(path);

        try
        {
            if (path.contains("pgm")) {
                ImageFileReader reader = new ImageFileReader();
                int rawImage[][] = reader.readPgmFile(path);
                tempPgmPixels = rawImage;

                int imageWidth = rawImage[0].length;
                int imageHeight = rawImage.length;

                image = new BufferedImage(imageWidth, imageHeight,
                        BufferedImage.TYPE_BYTE_GRAY);
                WritableRaster raster = image.getRaster();


                IntStream.range(0, imageHeight).forEach(
                        y ->
                        IntStream.range(0, imageWidth).forEach(
                                x -> raster.setSample(x, y, 0, rawImage[y][x])
                ));


                image.setData(raster);

            } else {
                image = ImageIO.read(file);
                ImageFileWriter writer = new ImageFileWriter();
                writer.saveToPGMfromBufferedImage(image, pgmPath);

                String imageName = imageNameForPath(pgmPath);
                ImageFileReader reader = new ImageFileReader();
                int rawImage[][] = reader.readPgmFile(imageName);
                tempPgmPixels = rawImage;

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        currentImage = image;
        return image;
    }

    public BufferedImage loadUrlImage(String path) {
        BufferedImage image = null;
        try {
            URL url = new URL(path);
            if (path.contains("pgm")) {
                String imageName = imageNameForPath(path);
                ReadableByteChannel rbc = Channels.newChannel(url.openStream());
                FileOutputStream fos = new FileOutputStream(imageName);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                ImageFileReader reader = new ImageFileReader();
                int rawImage[][] = reader.readPgmFile(imageName);
                tempPgmPixels = rawImage;
                localFilePath = imageName;
                int imageWidth = rawImage[0].length;
                int imageHeight = rawImage.length;

                image = new BufferedImage(imageWidth, imageHeight,
                        BufferedImage.TYPE_BYTE_GRAY);
                WritableRaster raster = image.getRaster();
                IntStream.range(0, imageHeight).forEach(
                        y ->
                                IntStream.range(0, imageWidth).forEach(
                                        x -> raster.setSample(x, y, 0, rawImage[y][x])
                ));

                image.setData(raster);
            } else {
                image = ImageIO.read(url);
                ImageFileWriter writer = new ImageFileWriter();
                writer.saveToPGMfromBufferedImage(image, pgmPath);

                String imageName = imageNameForPath(pgmPath);
                ImageFileReader reader = new ImageFileReader();
                int rawImage[][] = reader.readPgmFile(imageName);
                tempPgmPixels = rawImage;

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        currentImage = image;
        return image;
    }

    //SAVING
    public void saveImage() {
        ImageFileWriter imageFileWriter = new ImageFileWriter();

        char[][] ascii = ImageConverter.intensitiesToAscii(ImageManager.sharedManager().tempPgmPixels);

        imageFileWriter.saveToTxtFile(ascii, ImageManager.sharedManager().imageName() + txtExtension);
    }

    public void convertToGrayscale() {
        currentImage = Conversion.conversionToGreyscale(currentImage);
    }

    //HELPERS
    public String imageName() {
        Path p = Paths.get(localFilePath);
        return p.getFileName().toString();
    }
    public String imageNameForPath(String path) {
        Path p = Paths.get(path);
        return p.getFileName().toString();
    }
    public void clearData() {
        localFilePath = null;
        currentImage = null;
    }

}
