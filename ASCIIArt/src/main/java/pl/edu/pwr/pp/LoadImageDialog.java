package pl.edu.pwr.pp;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;

public class LoadImageDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton urlRadioButton;
    private JTextField urlLoadTextField;
    private JTextField localLoadTextField;
    private JRadioButton localRadioButton;
    private JButton loadImageButton;

    public boolean urlSelected() {
        return urlRadioButton.isSelected();
    }
    public boolean localSelected() {
        return localRadioButton.isSelected();
    }

    public String showDialog() {
        setBounds(500,500,500,300);
        setVisible(true);
        String path = "";
        if (localSelected()) {
            return localLoadTextField.getText();
        } else if (urlSelected()) {
            return urlLoadTextField.getText();
        }
        return path;
    }


    public LoadImageDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        loadImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //hddRadioButton.setSelected(true);
                JFileChooser chooser = new JFileChooser("/Users/wojtekfratczak/Desktop/JavaPWr/asciiart/ASCIIArt/src/main/resources/");
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "PGM Images", "pgm");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(getParent());
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    localLoadTextField.setText(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        localRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOK.setEnabled(urlSelected() || localSelected());
                if (urlRadioButton.isSelected()) {
                    urlRadioButton.setSelected(false);
                    urlLoadTextField.setEnabled(false);
                }
                loadImageButton.setEnabled(localRadioButton.isSelected());
                localLoadTextField.setEnabled(localRadioButton.isSelected());
            }
        });
        urlRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOK.setEnabled(urlSelected() || localSelected());
                if (localRadioButton.isSelected()) {
                    localRadioButton.setSelected(false);
                    localLoadTextField.setEnabled(false);
                    loadImageButton.setEnabled(false);
                }
                urlLoadTextField.setEnabled(urlRadioButton.isSelected());
            }
        });
    }

    private void onOK() {

        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        LoadImageDialog dialog = new LoadImageDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
