package pl.edu.pwr.pp;

import javax.swing.*;
import java.awt.image.BufferedImage;
//import org.apache.commons.io.FileUtils

/**
 * Created by wojtekfratczak on 09.05.2016.
 */
public class MainView {
    private JButton loadImageButton;
    private JButton saveImageButton;
    private JPanel mainPanel;
    private JPanel pgmImagePanel;
    private JLabel imageLabel;

    private ImageManager imageManager = ImageManager.sharedManager();

    static public void show() {
        JFrame frame = new JFrame("MainView");
        frame.setBounds(500,500,500,500);
        frame.setContentPane(new MainView().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public MainView() {

        loadImageButton.addActionListener( e -> {
            showLoadImageDialog();
        });

        saveImageButton.addActionListener( e -> {
                imageManager.saveImage();
        });
    }

    private void showLoadImageDialog() {
        ImageManager.sharedManager().clearData();
        saveImageButton.setEnabled(false);
        LoadImageDialog dialog = new LoadImageDialog();
        String imagePath = dialog.showDialog();
        if (imagePath == "") {
            BufferedImage image = null;
            ImageIcon imageIcon = new ImageIcon();
            imageLabel.setIcon(imageIcon);
            return;
        }

        if (dialog.localSelected()) {
            showLocalImage(imagePath);
        } else {
            showUrlImage(imagePath);
        }

    }

    private void showLocalImage(String path) {
        BufferedImage image = imageManager.loadLocalImage(path);
        ImageIcon imageIcon = new ImageIcon(image);
        imageLabel.setIcon(imageIcon);
        pgmImagePanel.setSize(imageLabel.getSize());
        saveImageButton.setEnabled(true);
    }

    private void showUrlImage(String path) {
        BufferedImage image = imageManager.loadUrlImage(path);
        ImageIcon imageIcon = new ImageIcon(image);
        imageLabel.setIcon(imageIcon);
        pgmImagePanel.setSize(imageLabel.getSize());
        saveImageButton.setEnabled(true);
    }

}
