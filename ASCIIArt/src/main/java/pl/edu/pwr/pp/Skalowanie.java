package pl.edu.pwr.pp;

import javax.swing.*;
import java.awt.event.*;

public class Skalowanie extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton szerokosc80RadioButton;
    private JRadioButton szerokoscEkranuRadioButton;
    private JRadioButton szerokosc160RadioButton;


    public String Skala()
    {
        setBounds(0,0,300,250);
        setVisible(true);
        if(szerokosc80RadioButton.isSelected())
            return "grayscaleRESIZED80.pgm";
        if(szerokosc160RadioButton.isSelected())
            return "grayscaleRESIZED160.pgm";
        if(szerokoscEkranuRadioButton.isSelected())
            return "grayscaleRESIZED.pgm";

        return "grayscale.pgm";
    }

    public Skalowanie() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        szerokosc80RadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOK.setEnabled(szerokosc80RadioButton.isSelected() || szerokosc160RadioButton.isSelected() || szerokoscEkranuRadioButton.isSelected());
                if(szerokosc160RadioButton.isSelected() || szerokoscEkranuRadioButton.isSelected()){
                    szerokosc160RadioButton.setEnabled(false);
                    szerokosc160RadioButton.setSelected(false);
                    szerokoscEkranuRadioButton.setEnabled(false);
                    szerokoscEkranuRadioButton.setEnabled(false);
                }
                szerokosc160RadioButton.setEnabled(true);
                szerokoscEkranuRadioButton.setEnabled(true);
            }
        });
        szerokosc160RadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOK.setEnabled(szerokosc80RadioButton.isSelected() || szerokosc160RadioButton.isSelected() || szerokoscEkranuRadioButton.isSelected());
                if(szerokosc80RadioButton.isSelected() || szerokoscEkranuRadioButton.isSelected()){
                    szerokosc80RadioButton.setEnabled(false);
                    szerokosc80RadioButton.setSelected(false);
                    szerokoscEkranuRadioButton.setEnabled(false);
                    szerokoscEkranuRadioButton.setEnabled(false);
                }
                szerokosc80RadioButton.setEnabled(true);
                szerokoscEkranuRadioButton.setEnabled(true);
            }
        });
        szerokoscEkranuRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOK.setEnabled(szerokosc80RadioButton.isSelected() || szerokosc160RadioButton.isSelected() || szerokoscEkranuRadioButton.isSelected());
                if(szerokosc160RadioButton.isSelected() || szerokosc80RadioButton.isSelected()){
                    szerokosc160RadioButton.setEnabled(false);
                    szerokosc160RadioButton.setSelected(false);
                    szerokosc80RadioButton.setEnabled(false);
                    szerokosc80RadioButton.setEnabled(false);
                }
                szerokosc160RadioButton.setEnabled(true);
                szerokosc80RadioButton.setEnabled(true);
            }
        });
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Skalowanie dialog = new Skalowanie();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
