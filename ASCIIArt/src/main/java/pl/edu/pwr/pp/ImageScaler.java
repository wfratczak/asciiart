package pl.edu.pwr.pp;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageScaler {
    public static BufferedImage resizeImage(BufferedImage originalImage, int type, int w, int h){
        BufferedImage resizedImage = new BufferedImage(w, h, type);
        Graphics2D g = resizedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(originalImage, 0 , 0, w, h, null);
        g.dispose();
        return resizedImage;
    }
}

