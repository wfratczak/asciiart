package pl.edu.pwr.pp;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
        //File file = new File("./'filename'.txt");
    try{
        PrintWriter writer = new PrintWriter(fileName);
        for (char[] chars : ascii) {
            writer.println(chars);
        }
        writer.close();
    } catch (FileNotFoundException e) {

    }


    }

    public void saveToPGMfromBufferedImage(BufferedImage BufferedImage, String fileName)throws IOException {
        // np. korzystajÄ…c z java.io.PrintWriter{
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        int rows = BufferedImage.getHeight();
        int columns = BufferedImage.getWidth();
        writer.println("P2");
        writer.println("#Creat by");
        writer.print(columns);
        writer.print(" ");
        writer.println(rows);
        writer.println(255);
        //writer.println();
        for(int i=0; i< BufferedImage.getHeight(); i++){
            for(int j = 0; j< BufferedImage.getWidth(); j++){
                Color mycolor = new Color(BufferedImage.getRGB(j, i));
                //cokolwiek - kazdy r,g,b -> ta sama wartosc
                int value = mycolor.getRed();
                writer.print(value);
                writer.print(" ");
            }
            writer.println();
        }
        writer.close();
    }
	
}
