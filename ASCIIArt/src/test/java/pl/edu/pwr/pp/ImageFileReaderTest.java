package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class ImageFileReaderTest {
	ImageFileReader imageReader = new ImageFileReader();
	File currentDirectory = new File(new File(".").getAbsolutePath());


	@Before
	public void setUp() {
		imageReader = new ImageFileReader();
	}

	@Test
	public void shouldReadSequenceFrom0To255GivenTestImage()throws IOException {
		// given
		String dir = "/src/test/resources/";

		File currentDirectory = new File(new File(".").getAbsolutePath());

		System.out.print(currentDirectory.getCanonicalPath() + dir + "testImage.pgm");

		// when
		int[][] intensities = null;
		try {
			intensities = imageReader.readPgmFile(currentDirectory.getCanonicalPath() + dir + "testImage.pgm");
			ImageManager.sharedManager().loadLocalImage(currentDirectory.getCanonicalPath() + dir + "testImage.pgm");
		} catch (URISyntaxException e) {
			Assert.fail("Should read the file");
		}
		// then
		int counter = 0;
		for (int[] row : intensities) {
			for (int intensity : row) {
				assertThat(intensity, is(equalTo(counter++)));
			}
		}
	}


	@Test
	public void shouldThrowExceptionWhenFileDontExist() {
		// given
		String fileName = "nonexistent.pgm";
		try {
			// when
			imageReader.readPgmFile(fileName);
			// then
			Assert.fail("Should throw exception");
		} catch (Exception e) {
			// tego się spodziewaliśmy, nic więcej nie trzeba sprawdzać
		}

	}
}
